var appIndicator = {};

(function(context, window) {

    //---------------------------------
    // PRIVADO
    //---------------------------------

    const log                       = console.log,
          _items                    = [],
          contenedor                = document.querySelector('#contenedor-trazabilidad'),
          contenedorDomRect         = contenedor.getBoundingClientRect(),
          trazabilidadBarraInferior = document.querySelector('#trazabilidad-barra-inferior'),
          trazabilidadBarra         = document.querySelector('#trazabilidad-barra'),

          type = (obj, fullClass) => {
            // get toPrototypeString() of obj (handles all types)
            // Early JS environments return '[object Object]' for null, so it's best to directly check for it.
            if (fullClass) {
                return (obj === null) ? '[object Null]' : Object.prototype.toString.call(obj);
            }
            if (obj == null) { return (obj + '').toLowerCase(); } // implicit toString() conversion
        
            var deepType = Object.prototype.toString.call(obj).slice(8,-1).toLowerCase();
            if (deepType === 'generatorfunction') { return 'function' }
        
            // Prevent overspecificity (for example, [object HTMLDivElement], etc).
            // Account for functionish Regexp (Android <=2.3), functionish <object> element (Chrome <=57, Firefox <=52), etc.
            // String.prototype.match is universally supported.

            return deepType.match(/^(array|bigint|date|error|function|generator|regexp|symbol)$/) ? deepType :
               (typeof obj === 'object' || typeof obj === 'function') ? 'object' : typeof obj;
          },

          initStyles = () => {
            styleBarInactive = {
              'background-color': colorInactive
            };

            styleBarActive = {
              'background-color': colorActive
            };

            styleCircleInactive = {
              'background-color': colorInactive
            };

            styleCircleActive = {
              'background-color': colorActive
            };

            styleItemInactive = {
              'color': colorInactive,
              'font-weight': 'normal',
              'border-bottom': 'none'
            };

            styleItemActive = {
              'color': colorActive,
              'font-weight': 'bold',
              'border-bottom': '2px solid ' + colorActive
            };
          },

          setActiveItem = (number) => {
            timeoutItemActive = window.setTimeout(() => {
              setActiveStyle(number);
            }, 50);
          },

          setActiveStyle = (number) => {
            if (items === null) {
              items = contenedor.querySelectorAll('.trazabilidad-item');
            }

            if (itemsCirculos === null) {
              itemsCirculos = contenedor.querySelectorAll('.trazabilidad-item-circulo')
            }

            Object.assign(itemsCirculos[number].style, styleCircleActive);

            items.forEach((item) => {
              Object.assign(item.style, styleItemInactive);
            });

            Object.assign(items[number].style, styleItemActive);
          },

          checkCircles = () => {
            itemsCirculos.forEach((element) => {
              var trazabilidadBarraDomRect = trazabilidadBarra.getBoundingClientRect()
                  posicionBarra            = trazabilidadBarra.offsetLeft + trazabilidadBarraDomRect.width;

              window.clearTimeout(timeoutItemActive);

              if (posicionBarra >= element.offsetLeft) {
                Object.assign(element.style, styleCircleActive);
              } else {
                Object.assign(element.style, styleCircleInactive);
              }
            });
          },

          goToItem = (number) => {
            if (number >= 0 && number < _items.length) {
              trazabilidadBarra.velocity({
                properties: {
                  width: (division * number) + '%'
                },
                options: {
                  duration: speedAnimation,
                  easing: 'swing',
                  queue: false,
                  progress: () => {
                    checkCircles();
                  },
                  complete: () => {
                    setActiveItem(number);
                  }
                }
              });
            }
            else {
              throw new Error('No existe el Item solicitado');
            }
          };

    var inicializado        = false,
        speedAnimation      = 400,
        cantidadItems       = 0,
        items               = null,
        itemsCirculos       = null,
        division            = 0,
        parte               = 0,
        timeoutItemActive   = null,
        colorInactive       = '#AAA',
        colorActive         = '#000',
        styleBarInactive    = null,
        styleBarActive      = null,
        styleCircleInactive = null,
        styleCircleActive   = null,
        styleItemInactive   = null,
        styleItemActive     = null;

    context.iniinicializar = (opciones) => {
        if (!inicializado) {
          if (type(opciones) === 'object' && Object.entries(opciones).length) {
            if (type(opciones.speedAnimation) === 'number') {
              speedAnimation = opciones.speedAnimation;
            }

            if (opciones.colorInactive !== undefined && type(opciones.colorInactive) === 'string') {
              colorInactive = opciones.colorInactive;
            }

            if (opciones.colorActive !== undefined && type(opciones.colorActive) === 'string') {
              colorActive = opciones.colorActive;
            }

            initStyles();

            if (type(opciones.styleBarInactive) === 'object' && Object.entries(opciones.styleBarInactive).length) {
              Object.assign(styleBarInactive, opciones.styleBarInactive);
            }

            if (type(opciones.styleBarActive) === 'object' && Object.entries(opciones.styleBarActive).length) {
              Object.assign(styleBarActive, opciones.styleBarActive);
            }

            if (type(opciones.styleCircleInactive) === 'object' && Object.entries(opciones.styleCircleInactive).length) {
              Object.assign(styleCircleInactive, opciones.styleCircleInactive);
            }

            if (type(opciones.styleCircleActive) === 'object' && Object.entries(opciones.styleCircleActive).length) {
              Object.assign(styleCircleActive, opciones.styleCircleActive);
            }

            if (type(opciones.styleItemInactive) === 'object' && Object.entries(opciones.styleItemInactive).length) {
              Object.assign(styleItemInactive, opciones.styleItemInactive);
            }

            if (type(opciones.styleItemActive) === 'object' && Object.entries(opciones.styleItemActive).length) {
              Object.assign(styleItemActive, opciones.styleItemActive);
            }
          }
          else {
            initStyles();
          }

          items = contenedor.querySelectorAll('.trazabilidad-item');

          Object.assign(trazabilidadBarraInferior.style, styleBarInactive);
          Object.assign(trazabilidadBarra.style, styleBarActive);

          if ((cantidadItems = items.length) > 0) {
            division = 100 / cantidadItems;
            parte    = division / 2;

            items.forEach((item, index) => {
              var itemDomRect             = item.getBoundingClientRect(),
                  percentageElement       = itemDomRect.width * 100 / contenedorDomRect.width,
                  percentageElementCircle = 0,
                  left                    = 0,
                  circle                  = null,
                  circleDomRect           = null,
                  styleCircle             = {};

              left = division * (index + 1) - parte;

              Object.assign(styleCircle, styleCircleInactive, { left: left + '%' });

              circle = document.createElement('div');
              circle.classList.add('trazabilidad-item-circulo');
              Object.assign(circle.style, styleCircle);

              contenedor.appendChild(circle);

              circleDomRect = circle.getBoundingClientRect();

              percentageElementCircle = circleDomRect.width * 100 / contenedorDomRect.width;

              circle.style.left = (left - (percentageElementCircle / 2)) + '%';

              if (index == 0) {
                trazabilidadBarra.style.left = left + '%';
                trazabilidadBarraInferior.style.left = left + '%';

                setActiveStyle(0);
              }
              else
                  if (cantidadItems == index + 1) {
                    trazabilidadBarraInferior.style.width = (division * index) + '%';
                  }

              item.style.left = (left - (percentageElement / 2)) + '%';

              _items.push({
                position: {
                  percentage: {
                    left: left
                  },
                  pixels: {
                    left: item.offsetLeft,
                    top: item.offsetTop
                  }
                },
                text: item.querySelector('.trazabilidad-item-texto').innerHTML
              });
            });

            itemsCirculos = document.querySelectorAll('.trazabilidad-item-circulo');
          }

          inicializado = true;
        }
        else {
          throw new Error('La app ya ha sido inicializada');
        }
    };

    context.obtenerItems = () => {
      return [ ..._items ];
    };

    context.goToItem = (number) => {
      goToItem(number);
    };

    context.readyDOM = (callbackFunction) => {
      if (document.readyState != 'loading')
        callbackFunction(event);
      else
        document.addEventListener("DOMContentLoaded", callbackFunction)
    };

})(appIndicator, window);


document.addEventListener('DOMContentLoaded', (event) => {

  appIndicator.iniinicializar({
    // speedAnimation: 1000,
    // colorInactive: 'pink',
    // colorActive: 'blueviolet',
    // styleBarInactive: {
    //   'background-color': 'pink'
    // },
    // styleBarActive: {
    //   'background-color': 'blueviolet'
    // },
    // styleCircleInactive: {
    //   'background-color': 'pink'
    // },
    // styleCircleActive: {
    //   'background-color': 'blueviolet'
    // },
    // styleItemInactive: {
    //   'color': 'gray',
    //   'font-weight': 'normal',
    //   'border-bottom': 'none'
    // },
    // styleItemActive: {
    //   'color': 'coral',
    //   'font-weight': 'bold',
    //   'border-bottom': '2px solid coral'
    // }
  });

});